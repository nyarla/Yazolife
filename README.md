Yazolife
========

  * Yet Another Gyazo-like client for Hatena Fotolife

何故作ったし？
--------------

Gyazo っぽい感じではてなフォトライフを使いたいなぁ、と思ってモノの探したら、
あんちぽさんの、

  * [kentaro/gyazolife](https://github.com/kentaro/gyazolife)

を見付けたんだけれども、いかんせん 5, 6 年前の代物だったし、
また、最新の Gyazo では Mac でスクリーンショットを撮る処理が進化してたりするので、
その辺りの処理を Gyazo から丸パクリという名のコピペしつつ、
あんちぽさんのコードをガン見しながら、モダーンな感じで作りました。

使い方
------

`Yazolife.app` ディレクリ は OSX の App Bundle 形式 (って言うんだっけ？) になってるので、
それをまるっと何かしらの方法でコピペした後、適当にダブルクリックするなりで起動すれば、多分使えると思います。

ただ、初回起動時には、スクリプト本体が依存する CPAN ライブラリをインストールするように一応はなってるので、
裏でエラーを吐いてない限りでは、一度目の起動にはそれなりに時間がかかります。たぶん。

また、はてなフォトライフの API を叩くのに必要なユーザー名と API Key については、
`Config::Pit` でごにょごにょしているので、

```
$ EDITOR=vim $appdir/Yazolife.app/Contents/MacOS/ppit set hatena.ne.jp
```

という感じで各位なんとかしてください。

あと最後に、

> 中身とかお前とか信用できねぇよ！！１

という方は、一応、中身見た上で使うかどうか判断してください。
Mac がぶっこわれた！！１ とか言われても、俺は知りませんえん。

ライセンス
----------

  * `Yazolife.app/Contents/Resoources/yazolife`
     * [gyazo/Gyazo](https://github.com/gyazo/Gyazo) からコードをパクって来たので GPL
     * オリジナルの Gyazo の諸権利は [NOTA Inc.](http://www.notainc.com/)に有り、それらのライセンスは GPL です。
     * (GPL のバージョンいくつか、はオリジナルのリポジトリに掲載されてないっぽい。2015年9月現在)
  * `Yazolife.app/Contents/*.sh`
     * この辺りは自分で書いたので MIT-license.
     * あと内部的には curl とか `cpanm` とか使ってます。が GPL 的なリンクになるのかちょっと不明……
     * まあこの辺りマズってたら変える予定です。はい。
  * `Yazolife.app/Contents/Info.plist`
     * [kentaro/gyazolife](https://github.com/kentaro/gyazolife) からパクって来たので MIT-license
     * オリジナルの `Info.plist` の諸権利は [あんちぽさん](https://github.com/kentaro) に有り、それらは MIT-license です。
     * ちなみにこのファイルはまるっとコピペしてきて改変したものです。
  * `Yazolife.app/Resources/icon.icns`
     * お手製。手作り。
     * (C) 2015 Naoki OKAMURA (nyarla) <nyarla@thotep.net>
     * Zen Brush で書いた文字を [Yosemite Icon Kakeru Yatsu](http://ls8h.com/yosemite-icon/)に食わせ、その生成物を [Online icon converter for png, ico, and icns - iConvertIcons](https://iconverticons.com/online/) で変換しますた。

作った人
--------

  * Naoki OKAMURA (nyarla) <nyarla@thotep.net>
  * Blog: [カラクリサイクル](http://the.nyarla.net/)
  * Twitter: [@nyarla](https://twitter.com/nyarla)
