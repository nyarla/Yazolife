#!/bin/sh

appdir="$(cd "$(dirname $0)"/.. && pwd)"

if [ ! -d $appdir/Resources/extlib ]; then
    cd $appdir/Resources
    /usr/bin/curl -L https://cpanmin.us | /usr/bin/perl - -n -L extlib local::lib WebService::Hatena::Fotolife Config::Pit YAML
fi

